'''Making a new data class 'Xydat' that contains two numpy arrays of the same length,
where one is dependent variable (ydata) and the other is the independent variable (xdata).
Objects store data, basic statistics, and have methods for common functions such as scatter plot, linear
regression.

Functions used:
regress2d: performs linear regression of y against x
makePseudo2d: makes random data to put into object

Also includes examples below the definitions in main().

To do:
Add error exception if inputs are incorrect type (!=numpy arrays)
Format how the object's values are printed to show four digits after decimal
Refactor code so functions are separate from playground.

'''
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import randn  

def regress2d(xdat,ydat, plot_on=1):
    '''Linear regression of y on x
        Usage: slope, yint=regress2d(xdat,ydat, plot_on=1)
          Input:
            xdat and ydat are nd arrays (defined in numpy)
            plot_on=1 if you want to plot the data/line
          Output:
            slope and yint are slope and y-intercept of best 
            (least square error) fit
    '''
    #Get slope of regression line 
    slopeNumArr=(xdat-xdat.mean())*(ydat-ydat.mean())
    slopeDenArr=(xdat-xdat.mean())**2
    slope=slopeNumArr.sum()/slopeDenArr.sum()
    
    #Get y-intercept of regression line
    yInt=ydat.mean()-slope*xdat.mean()

    if plot_on:
        markSize=3;
        plt.plot(xdat, ydat, 'bo', markersize=markSize)
        xLims=plt.xlim()  #x limits for plotting line below
        xLine=np.array(xLims) #x values for line
        yLine=yInt+xLine*slope  #y values for line
        plt.plot(xLine,yLine,'r')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Data (blue) and best linear fit (red)')

    return slope, yInt  #end of regress2d


def makePseudo2d(numPoints=10, mu=0, sigma=1):
    '''Create two pseudodata numpy arrays numPoints long each\n
    Usage: xdat, ydat=makePseudo2d(numPoints=10, mu=0, sigma=1)\n
    Creates 1d pseudodata with xdata set as range(numPoints)
    and ydata[i] is the same, but with gaussian noise (std deviation=sigma)
    and shifted by mu '''
    xdat=np.array(range(numPoints))
    ydat=xdat.copy()+sigma*randn(numPoints)+mu #copy to protect original xdat
    return xdat, ydat
    
    
class Xydat(object):
    '''2d data object with common methods for such data\n
        myDat=Xydat(xDat, yDat)\n
       xDat and yDat are numpy arrays.
       myDat stores data, means, variances, and has methods for 
       linear regression (linregress()) and scatter plots (scat())'''
    def __init__(self, xDat, yDat):
        '''Initialize object with data, means, sample variance'''  
        #To do: 
        #Check for valid type of data in
        #Add correlation coefficient (r)       
        self.xDat=xDat.copy()  #protecting original data
        self.yDat=yDat.copy()
        self.xMean=self.xDat.mean()
        self.yMean=self.yDat.mean()
        self.xVar=self.xDat.var(ddof=1)  #denominator degrees of freedom gives unbiased estimate
        self.yVar=self.yDat.var(ddof=1)
        
    def linregress(self, plot_on=1):
        '''Calculate slope and y intercept of regression line of Xydat; plots if plot_on'''
        slope, yInt=regress2d(self.xDat, self.yDat, plot_on)
        return slope, yInt
        
    def scat(self):
        '''Scatter plot of Xydat'''
        plt.scatter(self.xDat,self.yDat)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('y versus x values')
                      
    #output of print object
    def __str__(self):
        if self.xDat.size < 20:
            printStr="Xydat object\n  xDat: {}\n  yDat: {}".format(self.xDat, self.yDat)
        else:
            #would prefer to format this
            printStrX= str(self.xDat[0:5]).replace(']','') + " ... "  +  str(self.xDat[-5:]).replace('[','')
            printStrY= str(self.yDat[0:5]).replace(']','') + " ... "  +  str(self.yDat[-5:]).replace('[','')
            printStr="Xydat object\n  xDat:{}\n  yDat: {}".format(printStrX,printStrY)
        return printStr
        
####################################################    

####################
#Using the functions if part of main
####################
def main():
    #Generate pseudodata
    numPoints=100;
    sigma=3;
    mu=30;
    xdat, ydat=makePseudo2d(numPoints, mu, sigma)
    
    #Create xydat object
    my2d=Xydat(xdat,ydat)  
    
    #Show some of its properties 
    print "\n\nLet's look at my2d:"
    print my2d
    print "\nMean y value: {} ".format(my2d.yMean)
    print "Variance in the x values: {}".format(my2d.xVar)
    #plot it
    slope, yint=my2d.linregress()  


if __name__=="__main__":
    main()
        


