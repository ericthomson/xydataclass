xyDataClass 0.0.1

Define and use a Python data class 'Xydat' that contains two 
numpy arrays of the same length, where one is dependent
variable (ydata) and the other is the independent variable (xdata).

Xydat objects store x- and y-data, basic statistics (mean, variance), 
and have methods for common functions such as scatter
plot, linear regression.

Functions used:
-regress2d: performs linear regression of y against x
-makePseudo2d: makes random data to put into object

Also includes examples below the definitions in main().

thomson dot eric (gmail) 
===============

To do:

Add error exception if inputs are incorrect type (!=numpy arrays)
Format how the object's values are printed to show four digits after decimal
Refactor code so functions are separate from playground.